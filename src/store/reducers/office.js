import _ from 'lodash';

const initialState = {
  status: 'loading',
  error: null,
  documents: [],
  selectedDocumentIds: new Set(),
  diagnostics: {},
  emailAddress: undefined
};

const reducer = (state = initialState, action = {}) => {
  const {type, payload} = action;

  switch (type) {
    case 'OFFICE_INIT_PENDING':
      return {
        ...state,
        status: 'loading'
      };
    case 'OFFICE_INIT_FULFILLED':
      return {
        ...state,
        ...payload,
        status: 'success',
        error: null
      };
    case 'OFFICE_INIT_REJECTED':
      return {
        ...state,
        status: 'error',
        error: payload
      };
    case 'TOGGLE_DOCUMENT_SELECTION':
      const {documentId} = payload;
      let newSet = new Set(state.selectedDocumentIds);

      newSet.has(documentId)
        ? newSet.delete(documentId)
        : newSet.add(documentId);

      return {
        ...state,
        selectedDocumentIds: newSet
      };
    case 'OFFICE_DIAGNOSTICS_FULFILLED':
      return {
        ...state,
        diagnostics: payload
      };
    case 'SAVE_DOCUMENT_SETTINGS':
      return {
        ...state,
        documents: updateDocuments(state.documents, payload.docId, {link: payload.link})
      };
    case 'SET_DOCUMENT_NAME':
      return {
        ...state,
        documents: updateDocuments(state.documents, payload.docId, {name: payload.name})
      };
    case 'OFFICE_LOADED_OUTSIDE_CLIENT':
      return {
        ...state,
        status: 'error',
        error: 'Add-In loaded outside of Office client'
      };
    default:
      return state;
  }
};

function updateDocuments(documents, docId, props) {
  const index = _.findIndex(documents, doc => doc.id == docId);
  const document = documents[index];

  return [
    ...documents.slice(0, index),
    {
      ...document,
      ...props
    },
    ...documents.slice(index + 1)
  ]
}

export default reducer;
