﻿import {applyMiddleware, compose, createStore, combineReducers} from 'redux';
import thunk from 'redux-thunk';
import promiseMiddleware from 'redux-promise-middleware';
import { routeReducer as routing} from 'redux-simple-router';
import DevTools from '../components/App/DevTools';
import office from './reducers/office';

const reducers = combineReducers({
  office,
  routing
});

const middleware = [
  thunk,
  promiseMiddleware()
]

const enhancers = compose(applyMiddleware(...middleware), window.devToolsExtension
  ? window.devToolsExtension()
  : DevTools.instrument())

export default function configureStore(rootReducer = reducers, rootMiddleware = middleware, initialState = window.__INITIAL_STATE__, debug = __DEBUG__) {
  return createStore(rootReducer, initialState, enhancers)
}