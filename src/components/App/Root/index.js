import React, {Component, PropTypes} from 'react'
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import actions from 'store/actions/office'
import {Link} from 'react-router';

class Root extends Component {

    static contextTypes = {
        store: React.PropTypes.object
    }

    componentWillMount() {
        const {actions: {init}} = this.props
        init()
    }

    render() {
        const {error, status, children} = this.props

        if (status === 'loading' && !error) {
            return <div>Loading, please wait...</div>
        }

        return (
            <div>
                <div>
                    <Link to="/home">Home</Link>
                    &nbsp;|&nbsp;
                    <Link to="/settings">Settings</Link>
                </div>
                {children}
            </div>
        )
    }
}

export default connect(
    state => ({
        office: state.office
    }),
    dispatch => ({
        actions: bindActionCreators(actions, dispatch)
    })
)(Root);