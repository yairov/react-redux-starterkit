import React from 'react';
import { Route, IndexRedirect } from 'react-router';
import Root from './Root';
import Home from './Root/Home';
import Settings from './Root/Settings';

export default  (
  <Route component={Root} path='/'>
    <IndexRedirect to="home" />
    <Route component={Home} path='home'></Route>
    <Route component={Settings} path='settings'/>
  </Route>
);
