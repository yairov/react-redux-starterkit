import React from 'react';
import { Provider } from 'react-redux';
import { Router } from 'react-router';
import DevTools from './DevTools';
import routes from './routes';

const enableDevTools = false;

export default ({history, store, debug = false}) => (
  <Provider store={store}>
    <div>
      <Router history={history}>
        {routes}
      </Router>
      {enableDevTools && <DevTools />}
    </div>
  </Provider>
);