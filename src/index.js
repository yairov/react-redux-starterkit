import 'babel-polyfill'
import React from 'react';
import ReactDOM from 'react-dom';
import createMemoryHistory from 'history/lib/createMemoryHistory';
import {syncReduxAndRouter} from 'redux-simple-router';
import injectTapEventPlugin from 'react-tap-event-plugin'
import App from 'components/App';
import configureStore from 'store/configure';

const target = document.getElementById('root');
const history = createMemoryHistory();
const store = configureStore();

injectTapEventPlugin();
syncReduxAndRouter(history, store);

const node = (
    <App
        history={history}
        store={store}
        debug={__DEBUG__}
        debugExternal={__DEBUG_NW__}
    />
);

ReactDOM.render(node, target);