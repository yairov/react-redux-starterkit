import webpack              from 'webpack';
import WebpackDevMiddleware from 'webpack-dev-middleware';
import WebpackHotMiddleware from 'webpack-hot-middleware';
import historyApiFallback   from 'connect-history-api-fallback';
import express              from 'express';
import config               from '../config';
import webpackConfig        from './webpack/development_hot';
import https                from 'https';
import fs                   from 'fs';
import path                 from 'path';

const paths = config.get('utils_paths');
const compiler = webpack(webpackConfig);
const app = express();

app.use(historyApiFallback({
  verbose: false,
  rewrites: [{ from: /\/OfficeApp\/?$/, to: '/OfficeApp/index.html'}]
}));

app.use(WebpackDevMiddleware(compiler, {
  publicPath: webpackConfig.output.publicPath,
  contentBase: paths.project(config.get('dir_src')),
  hot: true,
  quiet: false,
  noInfo: true,
  lazy: false,
  stats: {
    colors: true
  }
}));

app.use(WebpackHotMiddleware(compiler));

const ssl_options = {
  // key: fs.readFileSync(path.join(__dirname, '../tools/sslkeys/www.robustico.com/robustico.com.key')),
  // cert: fs.readFileSync(path.join(__dirname, '../tools/sslkeys/www.robustico.com/robustico.com.crt')),
  // ca: fs.readFileSync(path.join(__dirname, '../tools/sslkeys//www.robustico.com/gd_bundle-g2-g1.crt'))
  pfx: fs.readFileSync(path.join(__dirname, '../tools/sslkeys/robustico.pfx'))
};

export default https.createServer(ssl_options, app);
