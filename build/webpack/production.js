import webpack           from 'webpack';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import webpackConfig     from './_base';

webpackConfig.module.loaders = webpackConfig.module.loaders.map(loader => {
  if (/less/.test(loader.test)) {
    if (loader.loader) {
      const [first, ...rest] = loader.loader.split('!');
      loader.loader = ExtractTextPlugin.extract(first, rest);
    }
  }

  return loader;
});

webpackConfig.plugins.push(
  new ExtractTextPlugin('./[name].css'),
  new webpack.optimize.DedupePlugin(),
  new webpack.optimize.UglifyJsPlugin({
    output: {
      comments: false
    },
    compress: {
      'unused': true,
      'dead_code': true,
      'properties': true,
      'drop_debugger': true,
      'drop_console': true
    }
  })
);

export default webpackConfig;
