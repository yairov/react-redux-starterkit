import webpack            from 'webpack';
import config             from '../../config';
import HtmlWebpackPlugin  from 'html-webpack-plugin';
import CopyWebpackPlugin  from 'copy-webpack-plugin';
import autoprefixer       from 'autoprefixer';
import yargs              from 'yargs';
import path               from 'path';

const paths = config.get('utils_paths');
const webapp = yargs.argv.webapp || 'debug';
const outpuPath = yargs.argv['output-path'] || paths.dist()

console.log(`> START webpack`);
console.log(`> using ${webapp} web`);
console.log(`> output path ${outpuPath}`)

const webpackConfig = {
  name: 'client',
  target: 'web',
  entry: {
    app: [
      paths.src('index.js'),
      paths.src('config.json')
    ],
    vendor: config.get('vendor_dependencies')
  },
  output: {
    filename: '[name].js',
    path: outpuPath,
    publicPath: '/OfficeApp/'
  },
  plugins: [
    new webpack.DefinePlugin(config.get('globals')),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new HtmlWebpackPlugin({
      template: paths.src('index.html'),
      hash: true,
      filename: 'index.html',
      inject: 'body'
    })
  ],
  resolve: {
    extensions: ['', '.js', '.jsx'],
    alias: config.get('utils_aliases')
  },
  resolveLoader: {
    modulesDirectories: ['./config', 'node_modules']
  },
  module: {
    preLoaders: [
      {
        test: /\.(js|jsx)$/,
        loader: 'baggage?[file].less&[file].scss'
      }
    ],
    loaders: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: 'babel',
        query: {
          stage: 0,
          optional: ['runtime']
        }
      },
      {
        test: /\.css$/,
        loader: "style-loader!css-loader"
      },
      {
        test: /\.less$/,
        loader: 'style-loader!css-loader?-autoprefixer!postcss-loader!less-loader'
      },
      {test: /\.(png|svg|gif)(\?.*)?$/, loader: 'url-loader?limit=100000'},
      /* eslint-disable */
      {
        test: /\.woff(\?.*)?$/,
        loader: "url-loader?prefix=fonts/&name=[path][name].[ext]&limit=10000&mimetype=application/font-woff"
      },
      {
        test: /\.woff2(\?.*)?$/,
        loader: "url-loader?prefix=fonts/&name=[path][name].[ext]&limit=10000&mimetype=application/font-woff2"
      },
      {
        test: /\.ttf(\?.*)?$/,
        loader: "url-loader?prefix=fonts/&name=[path][name].[ext]&limit=10000&mimetype=application/octet-stream"
      },
      {test: /\.eot(\?.*)?$/, loader: "file-loader?prefix=fonts/&name=fonts/[name].[ext]"},
      {test: /config.json/, loader: 'config-loader?target=' + webapp}
      /* eslint-enable */
    ]
  },
  postcss: [autoprefixer({browsers: ['last 5 versions']})]
};

export default webpackConfig;
