var path = require('path'),
  gulp = require('gulp'),
  gutil = require('gulp-util'),
  shelljs = require('shelljs'),
  _ = require('lodash'),
  Q = require('q'),
  exec = require('child_process').exec,
  rimraf = require('rimraf'),
  fs = require('fs');

// USAGE: gulp --target=<dev|test|production...> --env=<development|production> --message="<deployment message>"
// USAGE: gulp clean --target=<dev|test|production...>

gutil.log(gutil.env['env'])

var env = gutil.env['env'] || 'production'
var target = gutil.env['target'] || 'debug'

console.log(env, target)

function createFolderIfNotExist(path) {
  try {
    stats = fs.lstatSync(path);
  }
  catch (e) {
    fs.mkdirSync(path);
  }
}

function createConfiguration() {
  var deployDirectory = path.join(__dirname, '../', 'deploy');
  createFolderIfNotExist(deployDirectory);

  var paths = {
    msbuildFile: '"C:\\Program Files (x86)\\MSBuild\\14.0\\Bin\\MSBuild"',
    projectFolder: path.join(__dirname, '../'),
    deployFolder: path.join(deployDirectory, target)
  };

  createFolderIfNotExist(paths.deployFolder);

  var config = require('./deploy.json');

  if (!config.hasOwnProperty(target)) {
    throw new Error('Cannot find configuration for ' + target + '. Please check deploy.config file...')
  }

  var envConfig = config[target];

  return {
    target: target,
    appName: envConfig.azure.appName,
    gitUrl: gutil.template('https://${deployUsername}:${deployPassword}@${appName}.${gitBaseUrl}/${appName}.git', envConfig.azure),
    paths: _.extend(paths, {
      publishFolder: path.join(paths.deployFolder, 'publish'),
      gitFolder: path.join(paths.deployFolder, envConfig.azure.appName)
    })
  };
}

function execCommand(command, options) {
  var deferred = Q.defer();

  gutil.log(gutil.colors.magenta('executing command: '), command);

  setTimeout(function () {
    var stdout = '',
      stderr = '',
      child = exec(command, options);

    child.stdout.setEncoding('utf8');
    child.stdout.on('data', function (data) {
      stdout += data;
      gutil.log(data);
    });

    child.stderr.setEncoding('utf8');
    child.stderr.on('data', function (data) {
      stderr += data;
      gutil.log(data);
    });

    child.on('close', function (code) {
      code != 0
        ? deferred.reject()
        : deferred.resolve(stdout);
    });
  }, 1);

  return deferred.promise;
}

function runCommand(command) {
  var deferred = Q.defer();

  shelljs.exec(command, function (code) {
    code != 0
      ? deferred.reject()
      : deferred.resolve();
  });

  return deferred.promise;
}

function gitClone(config) {
  gutil.log(gutil.colors.magenta('Preparing git repository...'));

  try {
    var stats = fs.lstatSync(config.paths.gitFolder, fs.F_OK);
    if (stats.isDirectory()) {
      gutil.log(gutil.colors.magenta('Fetching...'));
      return execCommand('git pull', {cwd: config.paths.gitFolder});
    }
  }
  catch (e) { }

  return execCommand('git clone ' + config.gitUrl, {cwd: config.paths.deployFolder})
    .then(function () {
      return execCommand('git remote add azure ' + config.gitUrl, {cwd: config.paths.gitFolder})
    });
}

function nuget(config) {
  var solutionPath = path.join(config.paths.projectFolder, 'api/Owa.Api.sln'),
    packagesFolder = path.join(config.paths.projectFolder, 'api/packages'),
    msbuildCommand = [
      path.join(__dirname, 'nuget.exe'),
      'restore ' + solutionPath,
      '-o ' + packagesFolder
    ];

  gutil.log(gutil.colors.magenta('Restoring nuget packages...'), msbuildCommand.join(' '));

  return runCommand(msbuildCommand.join(' '));
}

function publish(config) {
  var publishPath = path.join(config.paths.deployFolder, 'publish'),
    msbuildCommand = [
    config.paths.msbuildFile,
    path.join(config.paths.projectFolder, 'api/Owa.Api.sln'),
    '/verbosity:m',
    '/p:AutoParameterizationWebConfigConnectionStrings=False',
    '/p:DeployOnBuild=true',
    '/p:Configuration=' + config.target + ';_PackageTempDir=' + publishPath
  ];

  gutil.log(gutil.colors.magenta('Publishing site...'));

  return rimraf(publishPath, function () {
    fs.mkdirSync(publishPath);
    return runCommand(msbuildCommand.join(' '));
  });
}

function webpack(config) {
  var webpackCommand = [
    'webpack',
    '--webapp=' + config.target,
    '--env=' + env,
    '--progress',
    '--output-path ' + path.join(config.paths.deployFolder, 'publish', 'OfficeApp').split('\\').join('\\\\')
  ];

  gutil.log(gutil.colors.magenta('Running webpack...'));

  return runCommand(webpackCommand.join(' '));
}

function assets(config) {
  var deferred = Q.defer();

  gutil.log(gutil.colors.magenta('Copying assets...'));
  var fileTypes = ['png', 'svg'],
    paths = fileTypes.map(function (type) {
      return path.join(config.paths.projectFolder, 'client/src') + '/**/*.' + type;
    });

  gulp.src(paths)
    .pipe(gulp.dest(path.join(config.paths.deployFolder, 'publish')))
    .on('end', function () {
      deferred.resolve();
    });

  return deferred.promise;
}

function copy(config) {
  var deferred = Q.defer();

  gutil.log(gutil.colors.magenta('Copying published folder to git folder...'));

  gulp.src(path.join(config.paths.deployFolder, 'publish') + '/**/*')
    .pipe(gulp.dest(path.join(config.paths.deployFolder, config.appName)))
    .on('end', function () {
      deferred.resolve();
    });

  return deferred.promise;
}

function gitPush(config) {
  var message = gutil.env['message'] || 'deployment ' + new Date().getTime(),
    options = {cwd: config.paths.gitFolder},
    commands = [
      'git add .',
      'git commit -m "' + message + '"',
      'git push azure master'
    ];

  gutil.log(gutil.colors.magenta('Pushing changes to git...'));

  var result = Q(config);

  commands.forEach(function (command) {
    result = result.then(function () {
      return execCommand(command, options);
    });
  });

  return result;
}

function prepare(config, cb) {
  gutil.log(gutil.colors.magenta('Preparing deployment folder...'));
  fs.realpath(config.paths.deployFolder, function (err, resolvedPath) {
    err ? fs.mkdir(config.paths.deployFolder, cb) : cb();
  });
};

gulp.task('clean', function () {
  var config = createConfiguration();
  gutil.log(gutil.colors.magenta('Cleaning deployment folder...'));

  rimraf(path.join(config.paths.deployFolder), function () {
    shelljs.mkdir('-p', config.paths.deployFolder);
  });
});

gulp.task('default', function () {
  var config = createConfiguration();

  prepare(config, function () {
    var result = Q(config),
      funcs = [gitClone, nuget, publish, webpack, assets, copy, gitPush];

    funcs.forEach(function (fn) {
      result = result.then(function () {
        return fn(config);
      });
    });
  });
});

gulp.task('quick', function () {
  var config = createConfiguration();

  prepare(config, function () {
    var result = Q(config),
      funcs = [gitClone, webpack, copy, gitPush];

    funcs.forEach(function (fn) {
      result = result.then(function () {
        return fn(config);
      });
    });
  });
});
