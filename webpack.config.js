require('babel/register');
var argv = require('yargs').argv;

process.env.NODE_ENV = argv.env || 'development';

const config   = require('./config');
module.exports = require('./build/webpack/' + config.get('env'));
