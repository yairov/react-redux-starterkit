var loaderUtils = require("loader-utils");
var Promise = require('bluebird');
var _ = require('lodash');
var exec = require('child-process-promise').exec;
var packageJson = require('../package.json');

var es = function(str) {
  return str.replace(/(?:\r\n|\r|\n)/g, ' ');
};

module.exports = function (source) {
  this.cacheable && this.cacheable();

  var me = this,
    callback = this.async(),
    query = loaderUtils.parseQuery(this.query),
    data = JSON.parse(source)[query.target],
    url = loaderUtils.interpolateName(this, "[name].[ext]", {content: data});

  data.target = query.target;
  data.version = packageJson.version;
  data.time = new Date();

    data = JSON.stringify(data);
    me.emitFile(url, data);

    callback(null, 'module.exports = ' + data);
};

